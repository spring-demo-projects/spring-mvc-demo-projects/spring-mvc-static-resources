<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome</title>
</head>
<body>
	<h1>Welcome to Spring MVC Static Resources</h1>

	<h4>This page is directly accessible to client without any
		controller, as it is located outside of WEB-INF directory</h4>
	<div>
		<p>
			Go To <a href="home">Page</a> via Controller and have requirement of
			static resource
		</p>
	</div>
</body>
</html>