<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>

<!-- Import external CSS file -->
<link rel="stylesheet" href="css/custom.css">

<!-- Import external JS file -->
<script type="text/javascript" src="js/custom.js"></script>

</head>
<body>

	<h2>This page required below static resources</h2>
	<ol>
		<li>CSS</li>
		<li>JS</li>
		<li>Image</li>
	</ol>
	<div class="error">
		<p>Client Browser will generate 4 request, details are given below</p>
		<ol>
			<li>For HTML Page :
				http://localhost:8080/springmvc_static_resources/home :-> Serve by
				Custom Controller</li>
			<li>For CSS file :
				http://localhost:8080/springmvc_static_resources/css/custom.css :->
				Serve by Dispatcher Servlet</li>
			<li>For JS file :
				http://localhost:8080/springmvc_static_resources/js/custom.js :->
				Serve by Dispatcher Servlet</li>
			<li>For Image file :
				http://localhost:8080/springmvc_static_resources/images/photo.PNG
				:-> Serve by Dispatcher Servlet</li>
		</ol>
	</div>
	<div>
		<img id="image-1" alt="Image Here" src="images/photo.PNG" />
	</div>

</body>
</html>