package com.cdac.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	@GetMapping(value = "/home", produces = "text/html")
	public String showHomePage() {
		return "homePage";
	}

}
